import React, { useState } from "react";

export function ClockToggle({ children, checked, onChange }) {
  const [time, setTime] = useState(checked ? new Date() : undefined);

  const kids = checked
    ? children
    : React.Children.map(children, (child) =>
        React.cloneElement(child, { time })
      );

  return (
    <div
      onClick={() => {
        onChange(!checked);
        setTime(checked ? new Date() : undefined);
      }}
      className={checked ? "clock-toggle" : "clock-toggle-disabled"}
    >
      {kids}

      {checked ? null : <div className="disabled-clock" />}
    </div>
  );
}
