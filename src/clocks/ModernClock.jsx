import React from "react";

import { Dial } from "./Dial";
import { range } from "../utils";

export function ModernClock(props) {
  return (
    <Dial backColor="#000" {...props}>
      <Background />

      <HourHand />
      <MinuteHand />
      <SecondHand />
    </Dial>
  );
}

const GRAY = "#8c8c9a";

const MONTHS = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

function SecondHand({ angle, radius }) {
  return (
    <g transform={`rotate(${angle})`}>
      <circle cx={radius - 12} cy={0} r={4} fill="red" />
    </g>
  );
}

function MinuteHand({ time }) {
  const minutes = time.getMinutes();
  const angle = (minutes * 180) / 30 - 180 / 2;

  return (
    <g transform={`rotate(${angle})`}>
      <polygon points="0,-2 90,0 0,2" fill={GRAY} />
      <circle cx={0} cy={0} r={3} fill={GRAY} />
    </g>
  );
}

function HourHand({ time }) {
  const hours = time.getHours();
  const angle = (hours * 180) / 6 - 180 / 2;

  return (
    <g transform={`rotate(${angle})`}>
      <polygon points="0,-2 70,0 0,2" fill={GRAY} />
      <circle cx={0} cy={0} r={3} fill={GRAY} />
    </g>
  );
}

function Background({ time, center, radius }) {
  const date = `${MONTHS[time.getMonth()]} ${time.getDate()}`;
  const eight = time.getHours() < 12 ? "8" : "20";

  return (
    <>
      {[...range(4)].map((i) => (
        <g key={i} transform={`rotate(${i * 90})`}>
          <line
            x1={0}
            y1={radius + 10}
            x2={0}
            y2={radius - 4}
            style={{ stroke: GRAY, strokeWidth: 3 }}
          />
        </g>
      ))}

      {[...range(12)].map((i) => (
        <g key={i} transform={`rotate(${i * 30})`}>
          <line
            x1={0}
            y1={radius + 10}
            x2={0}
            y2={radius}
            style={{ stroke: GRAY, strokeWidth: 1 }}
          />
        </g>
      ))}

      <text x={center.x - 192} y={center.y - 73} fill={GRAY}>
        {eight}
      </text>

      <text x={-18} y={45} fill="#f22a2a" style={{ fontSize: 12 }}>
        {date}
      </text>
    </>
  );
}
