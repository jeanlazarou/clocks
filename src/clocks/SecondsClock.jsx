import React from "react";

import { Dial } from "./Dial";

export function SecondsClock(props) {
  return (
    <Dial backColor="#000" {...props}>
      <SecondHand />
      <HourHand />
      <MinuteHand />
    </Dial>
  );
}

function polarToCartesian(distance, radians) {
  return {
    x: distance * Math.cos(radians),
    y: distance * Math.sin(radians),
  };
}

function arc(radius, angle) {
  var start = polarToCartesian(radius, angle);
  var end = polarToCartesian(radius, 0);

  var largeArcFlag = angle <= Math.PI ? "0" : "1";

  var d = [
    "M",
    start.x,
    start.y,
    "A",
    radius,
    radius,
    0,
    largeArcFlag,
    0,
    end.x,
    end.y,
  ].join(" ");

  return d;
}

function SecondHand({ time }) {
  const seconds = (time.getTime() / 1000) % 60.0;

  if (seconds === 0)
    return (
      <circle
        cx={0}
        cy={0}
        r={100}
        stroke="#446688"
        fill="none"
        strokeWidth="20"
      />
    );

  const angle = (seconds * Math.PI) / 30;

  return (
    <g transform={"rotate(-90)"}>
      <path d={arc(100, angle)} fill="none" stroke="#446688" strokeWidth="20" />
    </g>
  );
}

function MinuteHand({ time }) {
  const minutes = time.getMinutes();

  const x = Math.floor(Math.cos((minutes * Math.PI) / 30 - Math.PI / 2) * 105);
  const y = Math.floor(Math.sin((minutes * Math.PI) / 30 - Math.PI / 2) * 105);

  return (
    <text
      x={x}
      y={y}
      fill="white"
      style={{ fontSize: 18, textAnchor: "middle" }}
    >
      {minutes}
    </text>
  );
}

function HourHand({ time }) {
  const hours = time.getHours();

  const x = Math.floor(Math.cos((hours * Math.PI) / 6 - Math.PI / 2) * 70);
  const y = Math.floor(Math.sin((hours * Math.PI) / 6 - Math.PI / 2) * 70);

  return (
    <text
      x={x}
      y={y}
      fill="yellow"
      style={{ fontSize: 18, textAnchor: "middle" }}
    >
      {hours}
    </text>
  );
}
