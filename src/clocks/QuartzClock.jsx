import React, { useState } from "react";

const Tick = React.createContext(new Date());

let timerId = null;
let currentDelay = 0;

export function QuartzClock({ ticks, children }) {
  const [time, setTime] = useState(new Date());

  const delay = ticks ? 1000 : 50;

  if (currentDelay !== delay) {
    clearInterval(timerId);

    timerId = null;
  }

  if (timerId === null) {
    currentDelay = delay;

    timerId = setInterval(() => {
      setTime(new Date());
    }, delay);
  }

  return <Tick.Provider value={time}>{children}</Tick.Provider>;
}

export { Tick };
