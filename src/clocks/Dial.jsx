import React, { useContext } from "react";

import { Tick } from "./QuartzClock";

function secondsAngle(at) {
  const now = at.getTime();

  const midnight = new Date(at);
  midnight.setHours(0);
  midnight.setSeconds(0);
  midnight.setMinutes(0);
  midnight.setMilliseconds(0);

  let millis = now - midnight.getTime();

  const seconds = (millis / 1000) % 60;

  return (seconds * 180) / 30 - 90;
}

export function Dial({ children, time, backColor, className, absolute }) {
  const radius = 90;
  const center = { x: 120, y: 120 };

  const now = useContext(Tick);

  time = time ? time : now;

  const angle = secondsAngle(time);

  const kids = React.Children.map(children, (child) =>
    React.cloneElement(child, { angle, center, time, radius })
  );

  return (
    <div className={className}>
      <svg viewBox="0 0 240 240" preserveAspectRatio="xMidYMid meet">
        <rect x="0" y="0" width={240} height={240} fill={backColor} />

        {absolute ? (
          kids
        ) : (
          <g transform={`translate(${center.x}, ${center.y})`}>{kids}</g>
        )}
      </svg>
    </div>
  );
}
