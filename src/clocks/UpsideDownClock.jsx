import React from "react";

import { Dial } from "./Dial";
import { range } from "../utils";

export function UpsideDownClock(props) {
  return (
    <Dial backColor="#000" {...props}>
      <Background />

      <SecondHand />
    </Dial>
  );
}

function SecondHand({ angle, time }) {
  const hours = time.getHours();
  const minutes = time.getMinutes();

  return (
    <g transform={`rotate(${angle})`}>
      <line
        x1={15}
        y1={0}
        x2={50}
        y2={0}
        style={{ stroke: "yellow", strokeWidth: 2 }}
      />

      <text x={14} y={-6} fill="red" style={{ fontSize: 22 }}>
        {hours}:
      </text>
      <text x={48} y={20} fill="red" style={{ fontSize: 22 }}>
        {minutes}
      </text>
    </g>
  );
}

function Background({ radius }) {
  return [...range(4)].map((i) => (
    <g key={i} transform={`rotate(${i * 90})`}>
      <line
        x1={0}
        y1={radius + 10}
        x2={0}
        y2={radius + 5}
        style={{ stroke: "yellow", strokeWidth: 3 }}
      />
    </g>
  ));
}
