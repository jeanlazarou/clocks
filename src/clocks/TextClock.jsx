import React from "react";

import { Dial } from "./Dial";

export function TextClock(props) {
  return (
    <Dial backColor="#000" {...props} absolute>
      <Time />
    </Dial>
  );
}

const toS = (n) => (n < 10 ? "0" + n : n);

const format = (h, m, s) => `${toS(h)}:${toS(m)}:${toS(s)}`;

function Time({ time }) {
  const h = time.getHours();
  const m = time.getMinutes();
  const s = time.getSeconds();

  return (
    <>
      <text
        x={22}
        y={130}
        fill="#e3f41d"
        fillOpacity={0.17}
        style={{ fontSize: 52, fontFamily: "Digital7" }}
      >
        88:88:88
      </text>
      <text
        x={22}
        y={130}
        fill="#e3f41d"
        style={{ fontSize: 52, fontFamily: "Digital7" }}
      >
        {format(h, m, s)}
      </text>
    </>
  );
}
