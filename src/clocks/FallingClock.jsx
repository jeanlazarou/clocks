import React from "react";

import { Dial } from "./Dial";

export function FallingClock(props) {
  return (
    <Dial backColor="#000" {...props} absolute>
      <defs>
        <linearGradient id="blue" x1="0" y1="0" x2="0" y2="1">
          <stop offset="0%" stopColor="#426" />
          <stop offset="100%" stopColor="#33E" />
        </linearGradient>
        <linearGradient id="white" x1="0" y1="0" x2="0" y2="1">
          <stop offset="0%" stopColor="#eff" />
          <stop offset="100%" stopColor="#455" />
        </linearGradient>
      </defs>

      <Date />

      <HourHand />
      <MinuteHand />
      <SecondHand />
    </Dial>
  );
}

const format = (n) => (n < 10 ? "0" + n : n);

function Text({ value, n, x, y }) {
  return (
    <text
      x={x}
      y={y + 15 * (value % n)}
      fill="url(#white)"
      style={{ fontSize: 22 }}
    >
      {value}
    </text>
  );
}

function SecondHand({ time }) {
  const seconds = time.getSeconds();

  return <Text x={100} y={50} value={seconds} n={10} />;
}

function MinuteHand({ time }) {
  const minutes = time.getMinutes();

  return <Text x={60} y={50} value={minutes} n={10} />;
}

function HourHand({ time }) {
  const hours = time.getHours();

  return <Text x={20} y={110} value={hours} n={6} />;
}

function Date({ time }) {
  const day = time.getDate();
  const month = time.getMonth();
  const year = time.getFullYear();

  const formatted = `${format(month + 1)}/${format(day)}/${year}`;

  return (
    <text x={110} y={220} fill="url(#blue)" style={{ fontSize: 22 }}>
      {formatted}
    </text>
  );
}
