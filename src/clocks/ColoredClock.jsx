import React from "react";

import { Dial } from "./Dial";
import { range } from "../utils";

export function ColoredClock(props) {
  return (
    <Dial {...props}>
      <Background />

      <HourHand />
      <MinuteHand />
      <SecondHand />
    </Dial>
  );
}

const COLORS = [
  "#b90000",
  "#ec187c",
  "#80ff00",
  "blue",
  "#b000b0",
  "yellow",
  "pink",
  "gray",
  "#af070a",
  "white",
  "cyan",
  "orange",
];

function SecondHand({ angle, radius }) {
  return (
    <g transform={`rotate(${angle})`}>
      <line
        x1={-15}
        y1={0}
        x2={radius}
        y2={0}
        style={{ stroke: "red", strokeWidth: 1 }}
      />
      <circle cx={0} cy={0} r={4} fill="red" />
    </g>
  );
}

function MinuteHand({ time }) {
  const minutes = time.getMinutes();
  const angle = (minutes * 180) / 30 - 180 / 2;

  return (
    <g transform={`rotate(${angle})`}>
      <line
        x1={-10}
        y1={0}
        x2={80}
        y2={0}
        style={{ stroke: "whitesmoke", strokeWidth: 2 }}
      />
    </g>
  );
}

function HourHand({ time }) {
  const hours = time.getHours();
  const angle = (hours * 180) / 6 - 180 / 2;

  return (
    <g transform={`rotate(${angle})`}>
      <line
        x1={-10}
        y1={0}
        x2={60}
        y2={0}
        style={{ stroke: "#008000", strokeWidth: 2 }}
      />
    </g>
  );
}

function Background({ radius }) {
  return (
    <>
      {
        [...range(12)].reverse().reduce(
          (acc, i) => {
            const color = COLORS[i];

            const x = Math.cos(acc.angle) * radius;
            const y = Math.sin(acc.angle) * radius + 6;

            acc.hours.push(
              <text
                key={i}
                x={x}
                y={y}
                fill={color}
                style={{ fontSize: 20 }}
                textAnchor="middle"
                alignmentBaseline="middle"
              >
                {i + 1}
              </text>
            );

            acc.angle -= Math.PI / 6;

            return acc;
          },
          { angle: -Math.PI / 2, hours: [] }
        ).hours
      }
      {[...range(60)].map((i) => {
        return (
          <g key={i} transform={`rotate(${i * 6})`}>
            <line
              x1={0}
              y1={radius - 18}
              x2={0}
              y2={radius - 21}
              style={{ stroke: "#808040", strokeWidth: 1 }}
            />
          </g>
        );
      })}
    </>
  );
}
