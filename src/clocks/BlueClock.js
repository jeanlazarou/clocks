import React from "react";

import { Dial } from "./Dial";

export function BlueClock(props) {
  return (
    <Dial backColor="#eef" {...props}>
      <Background />

      <HourHand />
      <MinuteHand />
      <SecondHand />
    </Dial>
  );
}

function Background() {
  return <circle cx={0} cy={0} r={108} fill="#1293D8" />;
}

function Hand({ angle, width, radius }) {
  return (
    <g transform={`rotate(${angle})`}>
      <line
        x1={0}
        y1={0}
        x2={radius}
        y2={0}
        style={{ stroke: "#fff", strokeWidth: width, strokeLinecap: "round" }}
      />
    </g>
  );
}

function SecondHand({ angle, radius }) {
  return <Hand width={3} radius={radius} angle={angle} />;
}

function MinuteHand({ time, radius }) {
  const minutes = time.getMinutes();
  const angle = (minutes * 180) / 30 - 180 / 2;

  return <Hand width={6} radius={radius} angle={angle} />;
}

function HourHand({ time, radius }) {
  const hours = time.getHours();
  const angle = (hours * 180) / 6 - 180 / 2;

  return <Hand width={6} radius={radius * 0.667} angle={angle} />;
}
