import React from "react";

import { Dial } from "./Dial";

export function LineClock(props) {
  const styleLineBg = {
    stroke: "#7a8b8b",
    strokeWidth: 9,
    strokeLinecap: "round",
  };

  const x = 23 * 3 + 10;
  const x2 = 59 * 3 + 10;

  return (
    <Dial backColor="#2f2f4f" {...props} absolute>
      <line x1={80} y1={10} x2={x + 70} y2={x} style={styleLineBg} />
      <line x1={30} y1={10} x2={x2 + 20} y2={x2} style={styleLineBg} />

      <HourHand />
      <MinuteHand />
      <SecondHand />
    </Dial>
  );
}

function SecondHand({ time }) {
  const seconds = (time.getTime() / 1000) % 60.0;

  const x = seconds > 30 ? 210 - (seconds - 30) * 7 : seconds * 7;

  return (
    <line
      x1={15}
      y1={220}
      x2={x + 15}
      y2={220}
      style={{ stroke: "white", strokeWidth: 9, strokeLinecap: "round" }}
    />
  );
}

function MinuteHand({ time }) {
  const minutes = time.getMinutes();

  const x = minutes * 3 + 10;

  const styleLineBg = {
    stroke: "#dbdb70",
    strokeWidth: 9,
    strokeLinecap: "round",
  };

  return <line x1={30} y1={10} x2={x + 20} y2={x} style={styleLineBg} />;
}

function HourHand({ time }) {
  const hours = time.getHours();

  const x = hours * 3 + 10;

  const styleLineBg = {
    stroke: "#ff7f00",
    strokeWidth: 9,
    strokeLinecap: "round",
  };

  return <line x1={80} y1={10} x2={x + 70} y2={x} style={styleLineBg} />;
}
