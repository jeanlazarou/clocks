import React from "react";

import { Dial } from "./Dial";

export function BinaryClock(props) {
  return (
    <Dial backColor="#000" {...props} absolute>
      <Background />

      <HourHand />
      <MinuteHand />
      <SecondHand />
    </Dial>
  );
}

function SecondHand({ time }) {
  const seconds = time.getSeconds();

  return <Value key="sec" name="sec" x={170} value={seconds} />;
}

function MinuteHand({ time }) {
  const minutes = time.getMinutes();

  return <Value key="min" name="min" x={100} value={minutes} />;
}

function HourHand({ time }) {
  const hours = time.getHours();

  return <Value key="hour" name="hour" x={30} value={hours} />;
}

function Value({ name, x, value }) {
  return [
    <Leds
      key={name + "v1"}
      name={name + "v1"}
      x={x}
      value={Math.floor(value / 10)}
      color="yellow"
    />,
    <Leds
      key={name + "v2"}
      name={name + "v2"}
      x={x + 30}
      value={value % 10}
      color="yellow"
    />
  ];
}

function Background({ time }) {
  return (
    <>
      <rect
        x="10"
        y="10"
        rx={10}
        ry={10}
        width={220}
        height={220}
        fill="#0000"
        style={{ stroke: "gray", strokeWidth: 4 }}
      />

      <Leds name="h2" x={30} value={3} />
      <Leds name="h1" x={60} value={15} />
      <Leds name="m2" x={100} value={7} />
      <Leds name="m1" x={130} value={15} />
      <Leds name="s2" x={170} value={7} />
      <Leds name="s1" x={200} value={15} />
    </>
  );
}

function Leds({ color = "#2b2929", name, x, value }) {
  const n = Number(value).toString(2);

  return n
    .split("")
    .reverse()
    .reduce(
      (acc, char, i) => {
        acc.y -= 30;

        if (char === "0") return acc;

        acc.kids.push(<Led key={name + i} cx={x} cy={acc.y} color={color} />);

        return acc;
      },
      { y: 210, kids: [] }
    ).kids;
}

function Led({ cx, cy, color }) {
  return <circle cx={cx} cy={cy} r={5} fill={color} />;
}
