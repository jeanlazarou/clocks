import React, { useEffect, useState } from "react";

import "./SidebarHandle.css";

export function SidebarHandle({ sidePanelOpen, onClick }) {
  const [showHandle, setShowHandle] = useState(false);

  useEffect(() => {
    if (sidePanelOpen) {
      setShowHandle(false);
      return;
    }

    const handler = (ev) => {
      setShowHandle(ev.clientX < 30);
    };

    document.documentElement.addEventListener("mousemove", handler);

    return () =>
      document.documentElement.removeEventListener("mousemove", handler);
  }, [sidePanelOpen]);

  return (
    <div
      className={`sidebar-handle ${sidePanelOpen || showHandle ? "" : "hide"}`}
      onClick={() => onClick()}
    >
      <i className={sidePanelOpen ? "icon angle left" : "icon angle right"}></i>
    </div>
  );
}
