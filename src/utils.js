export function* range(n) {
  let i = 0;

  for (; i < n; i++) {
    yield i;
  }
}
