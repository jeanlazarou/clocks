import React from "react";
import { buttonGroup, useControls } from "leva";
import { Leva, LevaInputs } from "leva";

export function useSettings() {
  const { darkMode } = useControls("View", {
    darkMode: { value: true, label: "Dark Mode" },
  });
  const [{ shadowColor }, set] = useControls("View", () => ({
    shadowColor: {
      type: LevaInputs.STRING,
      value: "W",
      render: () => false,
    },
    Shadow: buttonGroup({
      W: () => set({ shadowColor: "W" }),
      R: () => set({ shadowColor: "R" }),
      B: () => set({ shadowColor: "B" }),
      P: () => set({ shadowColor: "P" }),
      Y: () => set({ shadowColor: "Y" }),
    }),
  }));
  const { compact, leftMenu } = useControls("View", {
    leftMenu: { value: true, label: "Left Menu" },
    compact: { value: false, label: "Compact" },
  });
  const { ticks } = useControls("Clocks", {
    ticks: { value: true, label: "Use Ticks" },
  });

  return { darkMode, shadowColor, compact, leftMenu, ticks };
}

export function Settings() {
  return <Leva hideCopyButton titleBar={false} />;
}
