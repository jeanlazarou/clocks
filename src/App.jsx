import React, { useEffect, useState } from "react";

import { AppFrame } from "./AppFrame";
import { Settings, useSettings } from "./Setting";

import { ClockToggle } from "./ClockToggle";

import { TextClock } from "./clocks/TextClock";
import { BlueClock } from "./clocks/BlueClock";
import { QuartzClock } from "./clocks/QuartzClock";
import { BinaryClock } from "./clocks/BinaryClock";
import { ColoredClock } from "./clocks/ColoredClock";
import { FallingClock } from "./clocks/FallingClock";
import { LineClock } from "./clocks/LineClock";
import { ModernClock } from "./clocks/ModernClock";
import { SecondsClock } from "./clocks/SecondsClock";
import { UpsideDownClock } from "./clocks/UpsideDownClock";

import "./App.css";

function allVisible() {
  return Array(9).fill(true);
}

function Menu({ children, visible, onToggle }) {
  return (
    <div>
      {React.Children.map(children, (child, i) => (
        <ClockToggle checked={visible[i]} onChange={() => onToggle(i)}>
          {child}
        </ClockToggle>
      ))}
    </div>
  );
}

function Content({ children, visible }) {
  return (
    <div className="content">
      {React.Children.map(children, (child, i) =>
        visible[i] ? child : <div className="empty" />
      )}
    </div>
  );
}

const SHADOW_COLORS = {
  W: "rgba(244, 237, 237, 0.75)",
  R: "rgba(213, 13, 13, 0.87)",
  B: "rgba(43, 18, 163, 0.75)",
  P: "rgba(115, 6, 88, 0.75)",
  Y: "rgba(212, 240, 14, 0.75)",
};

function App() {
  const { compact, darkMode, leftMenu, shadowColor, ticks } = useSettings();

  const [visible, setVisible] = useState(allVisible());

  useEffect(() => {
    document.body.classList.toggle("background-dark", darkMode);

    document
      .querySelectorAll(".empty")
      .forEach((clock) => clock.classList.toggle("empty-compact", compact));

    let shadow = darkMode
      ? `${SHADOW_COLORS[shadowColor]} 0px 54px 55px, ${SHADOW_COLORS[shadowColor]} 0px -12px 30px, ${SHADOW_COLORS[shadowColor]} 0px 4px 6px, ${SHADOW_COLORS[shadowColor]} 0px 12px 13px, ${SHADOW_COLORS[shadowColor]} 0px -3px 5px`
      : "6px 7px 13px 0px rgba(50, 50, 50, 0.75)";

    const clocks = document.querySelectorAll(".clock");
    clocks.forEach((clock) => (clock.style.boxShadow = shadow));

    shadow = darkMode
      ? "6px 7px 13px 0px " + SHADOW_COLORS["W"]
      : "6px 7px 13px 0px rgba(50, 50, 50, 0.75)";

    const mini = document.querySelectorAll(".mini-clock");
    mini.forEach((clock, i) => {
      if (visible[i]) clock.style.boxShadow = shadow;
    });
  }, [darkMode, visible, compact, shadowColor, leftMenu]);

  const toggle = (i) => {
    const copy = [...visible];
    copy[i] = !copy[i];
    setVisible(copy);
  };

  return (
    <AppFrame drawer={<Settings />}>
      <QuartzClock ticks={ticks}>
        {leftMenu && (
          <Menu visible={visible} onToggle={toggle}>
            <TextClock className="mini-clock" />
            <ModernClock className="mini-clock" />
            <ColoredClock className="mini-clock" />
            <BinaryClock className="mini-clock" />
            <FallingClock className="mini-clock" />
            <UpsideDownClock className="mini-clock" />
            <LineClock className="mini-clock" />
            <SecondsClock className="mini-clock" />
            <BlueClock className="mini-clock" />
          </Menu>
        )}

        <Content visible={visible}>
          <TextClock className="clock" />
          <ModernClock className="clock" />
          <ColoredClock className="clock" />
          <BinaryClock className="clock" />
          <FallingClock className="clock" />
          <UpsideDownClock className="clock" />
          <LineClock className="clock" />
          <SecondsClock className="clock" />
          <BlueClock className="clock" />
        </Content>
      </QuartzClock>
    </AppFrame>
  );
}

export default App;
