import React, { useState } from "react";

import { SidebarHandle } from "./SidebarHandle";

import "./AppFrame.css";

export function AppFrame({ drawer, children }) {
  const [sideVisible, setSideVisible] = useState(false);

  const onSidebarToggle = () => {
    setSideVisible(!sideVisible);
  };

  return (
    <>
      <nav id="drawer" className={sideVisible ? "drawer-open" : undefined}>
        {drawer}
      </nav>
      <SidebarHandle
        id="drawer-toggle"
        onClick={onSidebarToggle}
        sidePanelOpen={sideVisible}
      />

      <div id="app-content" className={sideVisible ? "app drawer-open" : "app"}>
        {children}
      </div>
    </>
  );
}
